package cn.douxxxd.util;

import com.alibaba.fastjson.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class SmsUtil {

    public static void getCode(String phone, String signName, String templateCode, String sysName) {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("phone", phone);
        parameters.put("signName", signName);
        parameters.put("templateCode", templateCode);
        parameters.put("sysName", sysName);
        sendPost("https://sms.common.vverp.com/api/getCode", parameters);
    }

    public static Boolean validateCode(String phone, String code) {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("phone", phone);
        parameters.put("code",code);
        String result = sendPost("https://sms.common.vverp.com/api/validateCode", parameters);
        System.out.println(result);
        JSONObject res = JSONObject.parseObject(result);
        return "0".equals(String.valueOf(res.get("code")));
    }

    public static void sendCustomizeMsg(String phone, String signName, String templateCode, String params, String sysName) {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("phone", phone);
        parameters.put("signName", signName);
        parameters.put("templateCode", templateCode);
        parameters.put("params", params);
        parameters.put("sysName", sysName);
        sendPost("https://sms.common.vverp.com/api/sendCustomizeMsg", parameters);
    }

    private static String sendPost(String url, Map<String, String> parameters) {
        String result = "";
        BufferedReader in = null;
        PrintWriter out = null;
        StringBuffer sb = new StringBuffer();
        String params = "";
        try {
            if (parameters.size() == 1) {
                for (String name : parameters.keySet()) {
                    sb.append(name).append("=").append(
                            java.net.URLEncoder.encode(parameters.get(name),
                                    "UTF-8"));
                }
                params = sb.toString();
            } else {
                for (String name : parameters.keySet()) {
                    sb.append(name).append("=").append(
                            java.net.URLEncoder.encode(parameters.get(name),
                                    "UTF-8")).append("&");
                }
                String temp_params = sb.toString();
                params = temp_params.substring(0, temp_params.length() - 1);
            }
            java.net.URL connURL = new java.net.URL(url);
            java.net.HttpURLConnection httpConn = (java.net.HttpURLConnection) connURL
                    .openConnection();
            httpConn.setRequestProperty("Accept", "*/*");
            httpConn.setRequestProperty("Connection", "Keep-Alive");
            httpConn.setRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            httpConn.setDoInput(true);
            httpConn.setDoOutput(true);
            out = new PrintWriter(httpConn.getOutputStream());
            out.write(params);
            out.flush();
            in = new BufferedReader(new InputStreamReader(httpConn
                    .getInputStream(), "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

}